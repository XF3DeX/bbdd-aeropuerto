-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: localhost    Database: aeropuerto_db
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.18.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aeroplano_empleado`
--

DROP TABLE IF EXISTS `aeroplano_empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aeroplano_empleado` (
  `aeroplano_id` bigint(20) NOT NULL,
  `empleado_mantenimiento_id` bigint(20) NOT NULL,
  PRIMARY KEY (`aeroplano_id`,`empleado_mantenimiento_id`),
  KEY `aeroplano_empleado_FK_1` (`empleado_mantenimiento_id`),
  CONSTRAINT `aeroplano_empleado_FK` FOREIGN KEY (`aeroplano_id`) REFERENCES `avion` (`id`),
  CONSTRAINT `aeroplano_empleado_FK_1` FOREIGN KEY (`empleado_mantenimiento_id`) REFERENCES `persona` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aeroplano_empleado`
--

LOCK TABLES `aeroplano_empleado` WRITE;
/*!40000 ALTER TABLE `aeroplano_empleado` DISABLE KEYS */;
INSERT INTO `aeroplano_empleado` VALUES (20,24),(22,26),(30,34),(44,48),(46,50),(48,53),(50,56),(52,59),(54,62),(56,65),(58,71),(60,183);
/*!40000 ALTER TABLE `aeroplano_empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `avion`
--

DROP TABLE IF EXISTS `avion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `avion` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) NOT NULL,
  `nombre` tinytext NOT NULL,
  `fecha_primer_vuelo` datetime NOT NULL,
  `numero_registro` bigint(20) NOT NULL,
  `modelo_id` bigint(20) NOT NULL,
  `hangar_id` bigint(20) NOT NULL,
  `dueño_id` bigint(20) DEFAULT NULL,
  `fecha_compra` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `avion_FK` (`modelo_id`),
  KEY `avion_FK_1` (`hangar_id`),
  KEY `avion_FK_2` (`dueño_id`),
  CONSTRAINT `avion_FK` FOREIGN KEY (`modelo_id`) REFERENCES `modelo` (`id`),
  CONSTRAINT `avion_FK_1` FOREIGN KEY (`hangar_id`) REFERENCES `hangar` (`id`),
  CONSTRAINT `avion_FK_2` FOREIGN KEY (`dueño_id`) REFERENCES `persona` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `avion`
--

LOCK TABLES `avion` WRITE;
/*!40000 ALTER TABLE `avion` DISABLE KEYS */;
INSERT INTO `avion` VALUES (2,'Avion','Avion 1','2021-01-19 20:25:00',1000,2,2,NULL,NULL),(3,'Avion','Avion 1','2021-01-19 20:25:00',1000,3,3,NULL,NULL),(4,'Avion','Avion 3iNf75iC','2021-01-19 21:00:03',1000,4,4,NULL,NULL),(5,'Avion','eOMtThyhVNLWUZNRcBaQKxI','2024-06-18 17:21:21',2349,5,5,NULL,NULL),(6,'Avion','eOMtThyhVNLWUZNRcBaQKxI','2024-06-18 17:21:21',2349,6,6,NULL,NULL),(19,'Avion','eOMtThyhVNLWUZNRcBaQKxI','2024-06-18 17:21:21',2349,13,13,NULL,NULL),(20,'Aeroplano','UMaAIKKIkknjWEXJUfPxxQHeWKEJ','2029-10-26 23:46:52',2000,13,13,23,'2015-01-25 06:04:44'),(21,'Avion','eOMtThyhVNLWUZNRcBaQKxI','2024-06-18 17:21:21',2349,14,14,NULL,NULL),(22,'Aeroplano','UMaAIKKIkknjWEXJUfPxxQHeWKEJ','2029-10-26 23:46:52',2000,14,14,25,'2015-01-25 06:04:44'),(29,'Avion','eOMtThyhVNLWUZNRcBaQKxI','2024-06-18 17:21:21',2349,18,18,NULL,NULL),(30,'Aeroplano','LlN','2029-10-26 23:46:52',2000,18,18,33,'2015-01-25 06:04:44'),(43,'Avion','eOMtThyhVNLWUZNRcBaQKxI','2024-06-18 17:21:21',2349,25,25,NULL,NULL),(44,'Aeroplano','UfzQhdgLLfDTDGspDb','2029-10-26 23:46:52',2789,25,25,47,'2015-01-25 06:04:44'),(45,'Avion','eOMtThyhVNLWUZNRcBaQKxI','2024-06-18 17:21:21',2349,26,26,NULL,NULL),(46,'Aeroplano','UfzQhdgLLfDTDGspDb','2029-10-26 23:46:52',2789,26,26,49,'2015-01-25 06:04:44'),(47,'Avion','eOMtThyhVNLWUZNRcBaQKxI','2024-06-18 17:21:21',2349,27,27,NULL,NULL),(48,'Aeroplano','qszYL','2029-10-26 23:46:52',2789,27,27,52,'2015-01-25 06:04:44'),(49,'Avion','eOMtThyhVNLWUZNRcBaQKxI','2024-06-18 17:21:21',2349,28,28,NULL,NULL),(50,'Aeroplano','qszYL','2029-10-26 23:46:52',2789,28,28,55,'2015-01-25 06:04:44'),(51,'Avion','eOMtThyhVNLWUZNRcBaQKxI','2024-06-18 17:21:21',2349,29,29,NULL,NULL),(52,'Aeroplano','qszYL','2029-10-26 23:46:52',2789,29,29,58,'2015-01-25 06:04:44'),(53,'Avion','eOMtThyhVNLWUZNRcBaQKxI','2024-06-18 17:21:21',2349,30,30,NULL,NULL),(54,'Aeroplano','qszYL','2029-10-26 23:46:52',2789,30,30,61,'2015-01-25 06:04:44'),(55,'Avion','eOMtThyhVNLWUZNRcBaQKxI','2024-06-18 17:21:21',2349,31,31,NULL,NULL),(56,'Aeroplano','qszYL','2029-10-26 23:46:52',2789,31,31,64,'2015-01-25 06:04:44'),(57,'Avion','eOMtThyhVNLWUZNRcBaQKxI','2024-06-18 17:21:21',2349,32,32,NULL,NULL),(58,'Aeroplano','qszYL','2029-10-26 23:46:52',2789,32,32,70,'2015-01-25 06:04:44'),(59,'Avion','eOMtThyhVNLWUZNRcBaQKxI','2024-06-18 17:21:21',2349,33,53,NULL,NULL),(60,'Aeroplano','qszYL','2029-10-26 23:46:52',2789,33,53,182,'2015-01-25 06:04:44');
/*!40000 ALTER TABLE `avion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hangar`
--

DROP TABLE IF EXISTS `hangar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hangar` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `numero` bigint(20) NOT NULL,
  `capacidad` bigint(20) NOT NULL,
  `latitud` float(10,6) NOT NULL,
  `longitud` float(10,6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hangar`
--

LOCK TABLES `hangar` WRITE;
/*!40000 ALTER TABLE `hangar` DISABLE KEYS */;
INSERT INTO `hangar` VALUES (2,2000,10,-34.914528,-57.964096),(3,2000,10,-34.914528,-57.964096),(4,2000,10,-34.914528,-57.964096),(5,3613,40,38.888733,-154.109879),(6,3,40,38.888733,-154.109879),(13,3613,40,38.888733,-154.109879),(14,3613,40,38.888733,-154.109879),(18,3613,40,38.888733,-154.109879),(25,3613,40,38.888733,-154.109879),(26,3613,40,38.888733,-154.109879),(27,3613,40,38.888733,-154.109879),(28,3613,40,38.888733,-154.109879),(29,3613,40,38.888733,-154.109879),(30,3613,40,38.888733,-154.109879),(31,3613,40,38.888733,-154.109879),(32,3613,40,38.888733,-154.109879),(35,529,9,-77.486778,-147.329178),(36,11,4,73.462608,-126.390755),(37,11,4,73.462608,-126.390755),(38,854,7,20.558186,-167.955154),(39,666,2,43.215500,158.930069),(40,711,7,31.751732,46.362095),(41,666,2,43.215500,158.930069),(42,666,2,43.215500,158.930069),(43,666,2,43.215500,158.930069),(44,666,2,43.215500,158.930069),(45,666,2,43.215500,158.930069),(46,666,2,43.215500,158.930069),(47,854,7,20.558186,-167.955154),(48,11,4,73.462608,-126.390755),(49,711,7,31.751732,46.362095),(50,11,4,73.462608,-126.390755),(51,11,4,73.462608,-126.390755),(52,127,6,38.888733,-154.109879),(53,3613,40,38.888733,-154.109879),(54,888,2,-6.897249,-21.923716),(55,11,4,73.462608,-126.390755),(56,127,6,38.888733,-154.109879),(57,127,6,38.888733,-154.109879),(58,127,6,38.888733,-154.109879),(59,127,6,38.888733,-154.109879),(60,127,6,38.888733,-154.109879),(61,127,6,38.888733,-154.109879),(62,431,6,-43.122612,-13.794499),(63,529,9,-77.486778,-147.329178),(64,431,6,-43.122612,-13.794499),(65,127,6,38.888733,-154.109879),(66,431,6,-43.122612,-13.794499),(67,431,6,-43.122612,-13.794499),(68,127,6,38.888733,-154.109879),(69,854,7,20.558186,-167.955154),(70,127,6,38.888733,-154.109879),(71,127,6,38.888733,-154.109879),(72,127,6,38.888733,-154.109879),(73,11,4,73.462608,-126.390755),(74,529,9,-77.486778,-147.329178),(75,529,9,-77.486778,-147.329178),(76,529,9,-77.486778,-147.329178),(77,529,9,-77.486778,-147.329178),(78,529,9,-77.486778,-147.329178),(79,127,6,38.888733,-154.109879),(80,529,9,-77.486778,-147.329178),(81,529,9,-77.486778,-147.329178),(82,11,4,73.462608,-126.390755),(83,127,6,38.888733,-154.109879),(84,127,6,38.888733,-154.109879),(85,529,9,-77.486778,-147.329178),(86,127,6,38.888733,-154.109879),(87,127,6,38.888733,-154.109879),(88,127,6,38.888733,-154.109879),(89,156,6,-76.453392,160.850052),(90,127,6,38.888733,-154.109879),(91,529,9,-77.486778,-147.329178),(92,156,6,-76.453392,160.850052);
/*!40000 ALTER TABLE `hangar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hangar_avion`
--

DROP TABLE IF EXISTS `hangar_avion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hangar_avion` (
  `hangar_id` bigint(20) NOT NULL,
  `avion_id` bigint(20) NOT NULL,
  PRIMARY KEY (`hangar_id`,`avion_id`),
  KEY `hangar_avion_FK_1` (`avion_id`),
  CONSTRAINT `hangar_avion_FK` FOREIGN KEY (`hangar_id`) REFERENCES `hangar` (`id`),
  CONSTRAINT `hangar_avion_FK_1` FOREIGN KEY (`avion_id`) REFERENCES `avion` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hangar_avion`
--

LOCK TABLES `hangar_avion` WRITE;
/*!40000 ALTER TABLE `hangar_avion` DISABLE KEYS */;
INSERT INTO `hangar_avion` VALUES (2,2),(3,3),(4,4),(5,5),(6,6),(13,19),(14,21),(18,29),(25,43),(26,45),(27,47),(28,49),(29,51),(30,53),(31,55),(32,57),(53,59);
/*!40000 ALTER TABLE `hangar_avion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modelo`
--

DROP TABLE IF EXISTS `modelo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modelo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `numero` bigint(20) NOT NULL,
  `capacidad` bigint(20) NOT NULL,
  `peso` decimal(10,0) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modelo`
--

LOCK TABLES `modelo` WRITE;
/*!40000 ALTER TABLE `modelo` DISABLE KEYS */;
INSERT INTO `modelo` VALUES (2,5000,100,1000000),(3,5000,100,1000000),(4,5000,100,1000000),(5,533,270,1282329),(6,533,270,1282329),(13,533,270,1282329),(14,533,270,1282329),(18,533,270,1282329),(25,533,270,1282329),(26,533,270,1282329),(27,533,270,1282329),(28,533,270,1282329),(29,533,270,1282329),(30,533,270,1282329),(31,533,270,1282329),(32,533,270,1282329),(33,533,270,1282329);
/*!40000 ALTER TABLE `modelo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `sueldo_fijo` decimal(10,0) DEFAULT NULL,
  `adicionales` decimal(10,0) DEFAULT NULL,
  `version` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=193 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona`
--

LOCK TABLES `persona` WRITE;
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
INSERT INTO `persona` VALUES (9,'EmplMantenimiento','Nombre1','Empleado',NULL,NULL,433),(10,'EmplMantenimiento','Federico','Empleado',NULL,NULL,0),(11,'EmplMantenimiento','Federico','Empleado',NULL,NULL,0),(12,'Persona','Federico','Pacheco',NULL,NULL,0),(13,'EmplMantenimiento','Federico','Empleado',NULL,NULL,0),(14,'Persona','Federico','Pacheco',NULL,NULL,0),(23,'Persona','yedUsFwdkelQbxeTeQOvaScfqIOOmaa','JxkyvRnL',NULL,NULL,0),(24,'EmplMantenimiento','RYtGKbgicZaHCBRQDSx','VLhpfQGTMDYpsBZxvfBoeygjb',NULL,NULL,0),(25,'Persona','yedUsFwdkelQbxeTeQOvaScfqIOOmaa','JxkyvRnL',NULL,NULL,0),(26,'EmplMantenimiento','RYtGKbgicZaHCBRQDSx','VLhpfQGTMDYpsBZxvfBoeygjb',179626,NULL,0),(33,'Persona','yedUsFwdkelQbxeTeQOvaScfqIOOmaa','JxkyvRnL',NULL,NULL,0),(34,'EmplMantenimiento','UMaAIKKIkknjWEXJUfPxxQHeWKEJ','dpHYZGhtgdntugzvvKAXLhM',179626,NULL,0),(47,'Persona','yedUsFwdkelQbxeTeQOvaScfqIOOmaa','JxkyvRnL',NULL,NULL,0),(48,'EmplMantenimiento','UMaAIKKIkknjWEXJUfPxxQHeWKEJ','dpHYZGhtgdntugzvvKAXLhM',179626,NULL,0),(49,'Persona','yedUsFwdkelQbxeTeQOvaScfqIOOmaa','JxkyvRnL',NULL,NULL,0),(50,'EmplMantenimiento','UMaAIKKIkknjWEXJUfPxxQHeWKEJ','dpHYZGhtgdntugzvvKAXLhM',179626,NULL,0),(51,'Piloto','LlN','gNfZBdyFGRajVfJNonEnOinZj',157872,47244,0),(52,'Persona','yedUsFwdkelQbxeTeQOvaScfqIOOmaa','JxkyvRnL',NULL,NULL,0),(53,'EmplMantenimiento','LlN','gNfZBdyFGRajVfJNonEnOinZj',179626,NULL,0),(54,'Piloto','UfzQhdgLLfDTDGspDb','QvBQYuxiXXVytGCxzVllpgTJKhRQq',157872,47244,0),(55,'Persona','yedUsFwdkelQbxeTeQOvaScfqIOOmaa','JxkyvRnL',NULL,NULL,0),(56,'EmplMantenimiento','LlN','gNfZBdyFGRajVfJNonEnOinZj',179626,NULL,0),(57,'Piloto','UfzQhdgLLfDTDGspDb','QvBQYuxiXXVytGCxzVllpgTJKhRQq',157872,47244,0),(58,'Persona','yedUsFwdkelQbxeTeQOvaScfqIOOmaa','JxkyvRnL',NULL,NULL,0),(59,'EmplMantenimiento','LlN','gNfZBdyFGRajVfJNonEnOinZj',179626,NULL,0),(60,'Piloto','UfzQhdgLLfDTDGspDb','QvBQYuxiXXVytGCxzVllpgTJKhRQq',157872,47244,0),(61,'Persona','yedUsFwdkelQbxeTeQOvaScfqIOOmaa','JxkyvRnL',NULL,NULL,0),(62,'EmplMantenimiento','LlN','gNfZBdyFGRajVfJNonEnOinZj',179626,NULL,0),(63,'Piloto','UfzQhdgLLfDTDGspDb','QvBQYuxiXXVytGCxzVllpgTJKhRQq',157872,47244,0),(64,'Persona','yedUsFwdkelQbxeTeQOvaScfqIOOmaa','JxkyvRnL',NULL,NULL,0),(65,'EmplMantenimiento','LlN','gNfZBdyFGRajVfJNonEnOinZj',179626,NULL,0),(66,'Piloto','UfzQhdgLLfDTDGspDb','QvBQYuxiXXVytGCxzVllpgTJKhRQq',157872,47244,0),(67,'Persona','Constantin','Orn',NULL,NULL,0),(68,'Persona','Norwood','Emard',NULL,NULL,0),(69,'Persona','Gregg','Sanford',NULL,NULL,0),(70,'Persona','yedUsFwdkelQbxeTeQOvaScfqIOOmaa','JxkyvRnL',NULL,NULL,0),(71,'EmplMantenimiento','LlN','gNfZBdyFGRajVfJNonEnOinZj',179626,NULL,0),(72,'Piloto','UfzQhdgLLfDTDGspDb','QvBQYuxiXXVytGCxzVllpgTJKhRQq',157872,47244,0),(73,'Persona','Connor','D\'Amore',NULL,NULL,0),(74,'Persona','eOMtThyhVNLWUZNRcBaQKxI','yedUsFwdkelQbxeTeQOvaScfqIOOmaa',NULL,NULL,0),(75,'Persona','eOMtThyhVNLWUZNRcBaQKxI','yedUsFwdkelQbxeTeQOvaScfqIOOmaa',NULL,NULL,0),(76,'Persona','eOMtThyhVNLWUZNRcBaQKxI','yedUsFwdkelQbxeTeQOvaScfqIOOmaa',NULL,NULL,0),(77,'Persona','jNBgpTmxx','pIoQM',NULL,NULL,0),(78,'Persona','JxkyvRnL','RYtGKbgicZaHCBRQDSx',NULL,NULL,0),(79,'Persona','VLhpfQGTMDYpsBZxvfBoeygjb','UMaAIKKIkknjWEXJUfPxxQHeWKEJ',NULL,NULL,0),(80,'Persona','dpHYZGhtgdntugzvvKAXLhM','LlN',NULL,NULL,0),(81,'Persona','gNfZBdyFGRajVfJNonEnOinZj','UfzQhdgLLfDTDGspDb',NULL,NULL,0),(82,'Persona','QvBQYuxiXXVytGCxzVllpgTJKhRQq','qszYL',NULL,NULL,0),(83,'Persona','YdvDhtAsLghPXAgtbprXPZkhnfLTBSX','sPB',NULL,NULL,0),(84,'Persona','Oxm','QIn',NULL,NULL,0),(85,'Persona','yIvpRgmgQsYEKk','AAAryjCRhLTuhnTodUewZQqaZErU',NULL,NULL,0),(86,'Persona','aofGvthLoyPLDADYzx','WoaMAzEEplqjJ',NULL,NULL,0),(87,'Persona','jNBgpTmxx','pIoQM',NULL,NULL,0),(88,'Persona','jNBgpTmxx','pIoQM',NULL,NULL,0),(89,'Persona','JxkyvRnL','RYtGKbgicZaHCBRQDSx',NULL,NULL,0),(90,'Persona','VLhpfQGTMDYpsBZxvfBoeygjb','UMaAIKKIkknjWEXJUfPxxQHeWKEJ',NULL,NULL,0),(91,'Persona','dpHYZGhtgdntugzvvKAXLhM','LlN',NULL,NULL,0),(92,'Persona','gNfZBdyFGRajVfJNonEnOinZj','UfzQhdgLLfDTDGspDb',NULL,NULL,0),(93,'Persona','QvBQYuxiXXVytGCxzVllpgTJKhRQq','qszYL',NULL,NULL,0),(94,'Persona','YdvDhtAsLghPXAgtbprXPZkhnfLTBSX','sPB',NULL,NULL,0),(95,'Persona','Oxm','QIn',NULL,NULL,0),(96,'Persona','yIvpRgmgQsYEKk','AAAryjCRhLTuhnTodUewZQqaZErU',NULL,NULL,0),(97,'Persona','aofGvthLoyPLDADYzx','WoaMAzEEplqjJ',NULL,NULL,0),(98,'Persona','jNBgpTmxx','pIoQM',NULL,NULL,0),(99,'Persona','jNBgpTmxx','pIoQM',NULL,NULL,0),(100,'Persona','JxkyvRnL','RYtGKbgicZaHCBRQDSx',NULL,NULL,0),(101,'Persona','VLhpfQGTMDYpsBZxvfBoeygjb','UMaAIKKIkknjWEXJUfPxxQHeWKEJ',NULL,NULL,0),(102,'Persona','dpHYZGhtgdntugzvvKAXLhM','LlN',NULL,NULL,0),(103,'Persona','gNfZBdyFGRajVfJNonEnOinZj','UfzQhdgLLfDTDGspDb',NULL,NULL,0),(104,'Persona','QvBQYuxiXXVytGCxzVllpgTJKhRQq','qszYL',NULL,NULL,0),(105,'Persona','YdvDhtAsLghPXAgtbprXPZkhnfLTBSX','sPB',NULL,NULL,0),(106,'Persona','Oxm','QIn',NULL,NULL,0),(107,'Persona','yIvpRgmgQsYEKk','AAAryjCRhLTuhnTodUewZQqaZErU',NULL,NULL,0),(108,'Persona','aofGvthLoyPLDADYzx','WoaMAzEEplqjJ',NULL,NULL,0),(109,'Persona','jNBgpTmxx','pIoQM',NULL,NULL,0),(110,'Persona','jNBgpTmxx','pIoQM',NULL,NULL,0),(111,'Persona','JxkyvRnL','RYtGKbgicZaHCBRQDSx',NULL,NULL,0),(112,'Persona','VLhpfQGTMDYpsBZxvfBoeygjb','UMaAIKKIkknjWEXJUfPxxQHeWKEJ',NULL,NULL,0),(113,'Persona','dpHYZGhtgdntugzvvKAXLhM','LlN',NULL,NULL,0),(114,'Persona','gNfZBdyFGRajVfJNonEnOinZj','UfzQhdgLLfDTDGspDb',NULL,NULL,0),(115,'Persona','QvBQYuxiXXVytGCxzVllpgTJKhRQq','qszYL',NULL,NULL,0),(116,'Persona','YdvDhtAsLghPXAgtbprXPZkhnfLTBSX','sPB',NULL,NULL,0),(117,'Persona','Oxm','QIn',NULL,NULL,0),(118,'Persona','yIvpRgmgQsYEKk','AAAryjCRhLTuhnTodUewZQqaZErU',NULL,NULL,0),(119,'Persona','aofGvthLoyPLDADYzx','WoaMAzEEplqjJ',NULL,NULL,0),(120,'Persona','jNBgpTmxx','pIoQM',NULL,NULL,0),(121,'Persona','jNBgpTmxx','pIoQM',NULL,NULL,0),(122,'Persona','JxkyvRnL','RYtGKbgicZaHCBRQDSx',NULL,NULL,0),(123,'Persona','VLhpfQGTMDYpsBZxvfBoeygjb','UMaAIKKIkknjWEXJUfPxxQHeWKEJ',NULL,NULL,0),(124,'Persona','dpHYZGhtgdntugzvvKAXLhM','LlN',NULL,NULL,0),(125,'Persona','gNfZBdyFGRajVfJNonEnOinZj','UfzQhdgLLfDTDGspDb',NULL,NULL,0),(126,'Persona','QvBQYuxiXXVytGCxzVllpgTJKhRQq','qszYL',NULL,NULL,0),(127,'Persona','YdvDhtAsLghPXAgtbprXPZkhnfLTBSX','sPB',NULL,NULL,0),(128,'Persona','Oxm','QIn',NULL,NULL,0),(129,'Persona','yIvpRgmgQsYEKk','AAAryjCRhLTuhnTodUewZQqaZErU',NULL,NULL,0),(130,'Persona','aofGvthLoyPLDADYzx','WoaMAzEEplqjJ',NULL,NULL,0),(131,'Persona','jNBgpTmxx','pIoQM',NULL,NULL,0),(132,'Persona','jNBgpTmxx','pIoQM',NULL,NULL,0),(133,'Persona','JxkyvRnL','RYtGKbgicZaHCBRQDSx',NULL,NULL,0),(134,'Persona','VLhpfQGTMDYpsBZxvfBoeygjb','UMaAIKKIkknjWEXJUfPxxQHeWKEJ',NULL,NULL,0),(135,'Persona','dpHYZGhtgdntugzvvKAXLhM','LlN',NULL,NULL,0),(136,'Persona','gNfZBdyFGRajVfJNonEnOinZj','UfzQhdgLLfDTDGspDb',NULL,NULL,0),(137,'Persona','QvBQYuxiXXVytGCxzVllpgTJKhRQq','qszYL',NULL,NULL,0),(138,'Persona','YdvDhtAsLghPXAgtbprXPZkhnfLTBSX','sPB',NULL,NULL,0),(139,'Persona','Oxm','QIn',NULL,NULL,0),(140,'Persona','yIvpRgmgQsYEKk','AAAryjCRhLTuhnTodUewZQqaZErU',NULL,NULL,0),(141,'Persona','aofGvthLoyPLDADYzx','WoaMAzEEplqjJ',NULL,NULL,0),(142,'Persona','jNBgpTmxx','pIoQM',NULL,NULL,0),(143,'Persona','aofGvthLoyPLDADYzx','WoaMAzEEplqjJ',NULL,NULL,0),(144,'Persona','JxkyvRnL','RYtGKbgicZaHCBRQDSx',NULL,NULL,0),(145,'Persona','VLhpfQGTMDYpsBZxvfBoeygjb','UMaAIKKIkknjWEXJUfPxxQHeWKEJ',NULL,NULL,0),(146,'Persona','dpHYZGhtgdntugzvvKAXLhM','LlN',NULL,NULL,0),(147,'Persona','gNfZBdyFGRajVfJNonEnOinZj','UfzQhdgLLfDTDGspDb',NULL,NULL,0),(148,'Persona','QvBQYuxiXXVytGCxzVllpgTJKhRQq','qszYL',NULL,NULL,0),(149,'Persona','YdvDhtAsLghPXAgtbprXPZkhnfLTBSX','sPB',NULL,NULL,0),(150,'Persona','Oxm','QIn',NULL,NULL,0),(151,'Persona','yIvpRgmgQsYEKk','AAAryjCRhLTuhnTodUewZQqaZErU',NULL,NULL,0),(152,'Persona','aofGvthLoyPLDADYzx','WoaMAzEEplqjJ',NULL,NULL,0),(153,'Persona','jNBgpTmxx','pIoQM',NULL,NULL,0),(154,'Persona','jNBgpTmxx','pIoQM',NULL,NULL,0),(155,'Persona','JxkyvRnL','RYtGKbgicZaHCBRQDSx',NULL,NULL,0),(156,'Persona','VLhpfQGTMDYpsBZxvfBoeygjb','UMaAIKKIkknjWEXJUfPxxQHeWKEJ',NULL,NULL,0),(157,'Persona','dpHYZGhtgdntugzvvKAXLhM','LlN',NULL,NULL,0),(158,'Persona','gNfZBdyFGRajVfJNonEnOinZj','UfzQhdgLLfDTDGspDb',NULL,NULL,0),(159,'Persona','QvBQYuxiXXVytGCxzVllpgTJKhRQq','qszYL',NULL,NULL,0),(160,'Persona','YdvDhtAsLghPXAgtbprXPZkhnfLTBSX','sPB',NULL,NULL,0),(161,'Persona','Oxm','QIn',NULL,NULL,0),(162,'Persona','yIvpRgmgQsYEKk','AAAryjCRhLTuhnTodUewZQqaZErU',NULL,NULL,0),(163,'Persona','aofGvthLoyPLDADYzx','WoaMAzEEplqjJ',NULL,NULL,0),(164,'Persona','jNBgpTmxx','pIoQM',NULL,NULL,0),(165,'Persona','Oxm','QIn',NULL,NULL,0),(166,'Persona','QvBQYuxiXXVytGCxzVllpgTJKhRQq','qszYL',NULL,NULL,1),(167,'Persona','JxkyvRnL','RYtGKbgicZaHCBRQDSx',NULL,NULL,1),(168,'Persona','VLhpfQGTMDYpsBZxvfBoeygjb','UMaAIKKIkknjWEXJUfPxxQHeWKEJ',NULL,NULL,1),(169,'Persona','eOMtThyhVNLWUZNRcBaQKxI','yedUsFwdkelQbxeTeQOvaScfqIOOmaa',NULL,NULL,0),(170,'Persona','VLhpfQGTMDYpsBZxvfBoeygjb','UMaAIKKIkknjWEXJUfPxxQHeWKEJ',NULL,NULL,1),(171,'Persona','JxkyvRnL','RYtGKbgicZaHCBRQDSx',NULL,NULL,1),(172,'Persona','JxkyvRnL','RYtGKbgicZaHCBRQDSx',NULL,NULL,1),(173,'Persona','VLhpfQGTMDYpsBZxvfBoeygjb','UMaAIKKIkknjWEXJUfPxxQHeWKEJ',NULL,NULL,1),(174,'Persona','JxkyvRnL','RYtGKbgicZaHCBRQDSx',NULL,NULL,1),(175,'Persona','VLhpfQGTMDYpsBZxvfBoeygjb','UMaAIKKIkknjWEXJUfPxxQHeWKEJ',NULL,NULL,1),(176,'Persona','VLhpfQGTMDYpsBZxvfBoeygjb','UMaAIKKIkknjWEXJUfPxxQHeWKEJ',NULL,NULL,1),(177,'Persona','dpHYZGhtgdntugzvvKAXLhM','LlN',NULL,NULL,1),(178,'Persona','VLhpfQGTMDYpsBZxvfBoeygjb','UMaAIKKIkknjWEXJUfPxxQHeWKEJ',NULL,NULL,1),(179,'Persona','JxkyvRnL','RYtGKbgicZaHCBRQDSx',NULL,NULL,1),(180,'Persona','JxkyvRnL','RYtGKbgicZaHCBRQDSx',NULL,NULL,1),(181,'Persona','dpHYZGhtgdntugzvvKAXLhM','LlN',NULL,NULL,1),(182,'Persona','yedUsFwdkelQbxeTeQOvaScfqIOOmaa','JxkyvRnL',NULL,NULL,0),(183,'EmplMantenimiento','LlN','gNfZBdyFGRajVfJNonEnOinZj',179626,NULL,0),(184,'Piloto','UfzQhdgLLfDTDGspDb','QvBQYuxiXXVytGCxzVllpgTJKhRQq',157872,47244,0),(185,'Persona','aofGvthLoyPLDADYzx','WoaMAzEEplqjJ',NULL,NULL,1),(186,'Persona','jNBgpTmxx','pIoQM',NULL,NULL,0),(187,'Persona','eOMtThyhVNLWUZNRcBaQKxI','yedUsFwdkelQbxeTeQOvaScfqIOOmaa',NULL,NULL,0),(188,'Persona','JxkyvRnL','RYtGKbgicZaHCBRQDSx',NULL,NULL,1),(189,'Persona','VLhpfQGTMDYpsBZxvfBoeygjb','UMaAIKKIkknjWEXJUfPxxQHeWKEJ',NULL,NULL,1),(190,'Persona','gNfZBdyFGRajVfJNonEnOinZj','UfzQhdgLLfDTDGspDb',NULL,NULL,0),(191,'Persona','VLhpfQGTMDYpsBZxvfBoeygjb','UMaAIKKIkknjWEXJUfPxxQHeWKEJ',NULL,NULL,1),(192,'Persona','gNfZBdyFGRajVfJNonEnOinZj','UfzQhdgLLfDTDGspDb',NULL,NULL,0);
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piloto_aeroplano`
--

DROP TABLE IF EXISTS `piloto_aeroplano`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piloto_aeroplano` (
  `piloto_id` bigint(20) NOT NULL,
  `aeroplano_id` bigint(20) NOT NULL,
  PRIMARY KEY (`piloto_id`,`aeroplano_id`),
  KEY `piloto_aeroplano_FK_1` (`aeroplano_id`),
  CONSTRAINT `piloto_aeroplano_FK` FOREIGN KEY (`piloto_id`) REFERENCES `persona` (`id`),
  CONSTRAINT `piloto_aeroplano_FK_1` FOREIGN KEY (`aeroplano_id`) REFERENCES `avion` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piloto_aeroplano`
--

LOCK TABLES `piloto_aeroplano` WRITE;
/*!40000 ALTER TABLE `piloto_aeroplano` DISABLE KEYS */;
INSERT INTO `piloto_aeroplano` VALUES (51,46),(54,48),(57,50),(60,52),(63,54),(66,56),(72,58),(184,60);
/*!40000 ALTER TABLE `piloto_aeroplano` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reporte`
--

DROP TABLE IF EXISTS `reporte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reporte` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `aeroplano_id` bigint(20) NOT NULL,
  `fecha` datetime NOT NULL,
  `horas_incurridas` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `reporte_FK` (`aeroplano_id`),
  CONSTRAINT `reporte_FK` FOREIGN KEY (`aeroplano_id`) REFERENCES `avion` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reporte`
--

LOCK TABLES `reporte` WRITE;
/*!40000 ALTER TABLE `reporte` DISABLE KEYS */;
INSERT INTO `reporte` VALUES (1,44,'2024-06-18 00:00:00',14),(2,46,'2024-06-18 00:00:00',14),(3,48,'2024-06-18 00:00:00',14),(4,50,'2024-06-18 00:00:00',14),(5,52,'2024-06-18 00:00:00',14),(6,54,'2024-06-18 00:00:00',14),(7,56,'2024-06-18 00:00:00',14),(8,58,'2024-06-18 00:00:00',14),(9,60,'2024-06-18 00:00:00',14);
/*!40000 ALTER TABLE `reporte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tarea`
--

DROP TABLE IF EXISTS `tarea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tarea` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` tinytext NOT NULL,
  `descripcion` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tarea`
--

LOCK TABLES `tarea` WRITE;
/*!40000 ALTER TABLE `tarea` DISABLE KEYS */;
INSERT INTO `tarea` VALUES (1,'RYtGKbgicZaHCBRQDSx','VLhpfQGTMDYpsBZxvfBoeygjb'),(7,'RYtGKbgicZaHCBRQDSx','VLhpfQGTMDYpsBZxvfBoeygjb'),(8,'RYtGKbgicZaHCBRQDSx','VLhpfQGTMDYpsBZxvfBoeygjb'),(9,'RYtGKbgicZaHCBRQDSx','VLhpfQGTMDYpsBZxvfBoeygjb'),(10,'UMaAIKKIkknjWEXJUfPxxQHeWKEJ','dpHYZGhtgdntugzvvKAXLhM'),(11,'RYtGKbgicZaHCBRQDSx','VLhpfQGTMDYpsBZxvfBoeygjb'),(12,'UMaAIKKIkknjWEXJUfPxxQHeWKEJ','dpHYZGhtgdntugzvvKAXLhM'),(13,'RYtGKbgicZaHCBRQDSx','VLhpfQGTMDYpsBZxvfBoeygjb'),(14,'UMaAIKKIkknjWEXJUfPxxQHeWKEJ','dpHYZGhtgdntugzvvKAXLhM'),(15,'RYtGKbgicZaHCBRQDSx','VLhpfQGTMDYpsBZxvfBoeygjb'),(16,'UMaAIKKIkknjWEXJUfPxxQHeWKEJ','dpHYZGhtgdntugzvvKAXLhM'),(17,'RYtGKbgicZaHCBRQDSx','VLhpfQGTMDYpsBZxvfBoeygjb'),(18,'UMaAIKKIkknjWEXJUfPxxQHeWKEJ','dpHYZGhtgdntugzvvKAXLhM'),(19,'RYtGKbgicZaHCBRQDSx','VLhpfQGTMDYpsBZxvfBoeygjb'),(20,'UMaAIKKIkknjWEXJUfPxxQHeWKEJ','dpHYZGhtgdntugzvvKAXLhM'),(21,'RYtGKbgicZaHCBRQDSx','VLhpfQGTMDYpsBZxvfBoeygjb');
/*!40000 ALTER TABLE `tarea` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tarea_empleado`
--

DROP TABLE IF EXISTS `tarea_empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tarea_empleado` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tarea_id` bigint(20) NOT NULL,
  `empleado_mantenimiento_id` bigint(20) NOT NULL,
  `reporte_id` bigint(20) NOT NULL,
  `fecha` date NOT NULL,
  `hora_desde` time NOT NULL,
  `hora_hasta` time NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tarea_empleado_FK` (`tarea_id`),
  KEY `tarea_empleado_FK_1` (`empleado_mantenimiento_id`),
  KEY `tarea_empleado_FK_2` (`reporte_id`),
  CONSTRAINT `tarea_empleado_FK` FOREIGN KEY (`tarea_id`) REFERENCES `tarea` (`id`),
  CONSTRAINT `tarea_empleado_FK_1` FOREIGN KEY (`empleado_mantenimiento_id`) REFERENCES `persona` (`id`),
  CONSTRAINT `tarea_empleado_FK_2` FOREIGN KEY (`reporte_id`) REFERENCES `reporte` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tarea_empleado`
--

LOCK TABLES `tarea_empleado` WRITE;
/*!40000 ALTER TABLE `tarea_empleado` DISABLE KEYS */;
INSERT INTO `tarea_empleado` VALUES (2,7,48,1,'2029-10-26','17:21:21','23:46:52'),(3,8,50,2,'2029-10-26','17:21:21','23:46:52'),(4,9,53,3,'2029-10-26','17:21:21','23:46:52'),(5,10,56,4,'2015-01-25','06:04:44','14:36:39'),(6,11,56,4,'2029-10-26','17:21:21','23:46:52'),(7,12,59,5,'2015-01-25','06:04:44','14:36:39'),(8,13,59,5,'2029-10-26','17:21:21','23:46:52'),(9,14,62,6,'2015-01-25','06:04:44','14:36:39'),(10,15,62,6,'2029-10-26','17:21:21','23:46:52'),(11,16,65,7,'2015-01-25','06:04:44','14:36:39'),(12,17,65,7,'2029-10-26','17:21:21','23:46:52'),(13,18,71,8,'2015-01-25','06:04:44','14:36:39'),(14,19,71,8,'2029-10-26','17:21:21','23:46:52'),(15,20,183,9,'2015-01-25','06:04:44','14:36:39'),(16,21,183,9,'2029-10-26','17:21:21','23:46:52');
/*!40000 ALTER TABLE `tarea_empleado` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-01 21:41:49
