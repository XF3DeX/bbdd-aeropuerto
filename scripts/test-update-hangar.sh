# Probamos actualizar el mismo hangar 3 veces
# Vemos que por el mecanismo de locking pesimista algunas request pueden fallar 

update_hangar() {
    curl -X PUT 'http://localhost:8080/hangar/6'  \
        --header 'Content-Type: application/json' \
        --data-raw '{"numero": "'$1'"}' \
        --fail \
        -s -o /dev/null

    if [ $? -ne 0 ]; then
        echo "Fallo al actualizar $1"
    else
        echo "Se actualizó $1"
    fi
}

export -f update_hangar

seq 1 3 | xargs -n 1 -P 10 -I {} bash -c 'update_hangar $@' _ {}
