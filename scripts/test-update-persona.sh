# Ejecuta al mismo tiempo 5 requests para tratar de modificar el nombre de la persona 9.
# Se puede notar que algunas requests fallan porque se activa
# la protección de locking optimista (mirar el log de la aplicación).

update_persona() {
    curl -X PUT 'http://localhost:8080/persona/9'  \
        --header 'Content-Type: application/json' \
        --data-raw '{"nombre": "Nombre'$1'"}' \
        --fail \
        -s -o /dev/null

    if [ $? -ne 0 ]; then
        echo "Fallo al actualizar $1"
    else
        echo "Se actualizó $1"
    fi
}

export -f update_persona

seq 1 5 | xargs -n 1 -P 10 -I {} bash -c 'update_persona $@' _ {}
