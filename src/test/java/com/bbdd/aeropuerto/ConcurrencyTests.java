package com.bbdd.aeropuerto;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.assertj.core.util.Lists;
import org.glassfish.jersey.internal.guava.ThreadFactoryBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;
import com.bbdd.aeropuerto.concurrency.FindByIdCallable;
import com.bbdd.aeropuerto.concurrency.UpdateByIdCallable;
import com.bbdd.aeropuerto.model.Entity;
import com.bbdd.aeropuerto.model.Hangar;
import com.bbdd.aeropuerto.model.Persona;
import com.bbdd.aeropuerto.service.HangarService;
import com.bbdd.aeropuerto.service.PersonaService;

/**
 * Los test de concurrencia más que basarse en los asserts para testear la funcionalidad son una forma de recrear la situación que se quiere probar,
 * con el objetivo de poder mirar el log y entender qué es lo que pasa.
 */
@SpringBootTest
class ConcurrencyTests {
	
	@Autowired
	private PersonaService personaService;
	
	@Autowired
	private HangarService hangarService;
	
	private ExecutorService selectExecutorService;
	
	private ExecutorService updateExecutorService;
	
	@BeforeEach
	private void init() {
		selectExecutorService = Executors.newFixedThreadPool(10, new ThreadFactoryBuilder().setNameFormat("select-%d").build());
		updateExecutorService = Executors.newFixedThreadPool(10, new ThreadFactoryBuilder().setNameFormat("update-%d").build());
	}
	
	@AfterEach
	private void destroy() {
		selectExecutorService.shutdown();
		updateExecutorService.shutdown();
	}
	
	/**
	 * Test básico en donde probamos leer 10 veces la misma persona.
	 */
	@Test
	void basicReadTest() {
		List<Future<Persona>> futureList = Lists.newArrayList();
		Persona entity = this.createAndSavePersona();
		Long id = entity.getId();
		int readSize = 10;
		
		for (int i = 0; i < readSize; i++) {
			Future<Persona> future = selectExecutorService.submit(new FindByIdCallable<>(personaService, id));
			futureList.add(future);
		}
		
		List<Persona> personas = resolveFutures(futureList);
		Assert.isTrue(personas.size() == readSize);
	}
	
	/**
	 * En este test tenemos una modificación (lectura + modificación) de la entidad, y al mismo tiempo lecturas sobre la misma entidad.
	 * Si vemos el log podemos ver que una vez que llega el primer select sobre los datos, automáticamente solo las operaciones de lecturas
	 * van a estar permitidas sobre los datos, con lo cual todas las actualizaciones van a quedar esperando hasta que el lock para la modificación
	 * de los datos esté libre.
	 *
	 * Esto hace que <b>por lo general</b> primero se ejecuten todas los select y luego todos los updates.
	 *
	 * Para entender mejor qué está pasando se puede ver la ejecución de las sentencias SQL según el nombre del thread en el que se ejecutó.
	 * En este caso donde terminan quedando 3 updates al final de la ejecución, vamos a tener 2 fallos de lock, ya que la transacción se configuró con
	 * isolation level SERIALIZABLE, su ejecución se supone que debería ser como ejecutar secuencialmente SELECT+UPDATE, pero vemos que eso se rompió,
	 * ya que otras operaciones se metieron entre SELECT y UPDATE.
	 * @throws InterruptedException
	 */
	@Test
	void pesimisticReadTest() {
		Hangar hangar = this.createAndSaveHangar();
		List<Future<Hangar>> selectFutures = Lists.newArrayList();
		List<Future<Hangar>> updateFutures = Lists.newArrayList();
		Long id = hangar.getId();
		
		for (int i = 0; i < 3; i++) {
			updateFutures.add(updateExecutorService.submit(new UpdateByIdCallable<>(hangarService, id, createHangar())));
			selectFutures.add(selectExecutorService.submit(new FindByIdCallable<>(hangarService, id)));
		}
		
		List<Hangar> updatedHangars = resolveFutures(updateFutures);
		List<Hangar> selectedHangars = resolveFutures(selectFutures);
		Assertions.assertEquals(3, selectedHangars.size());
		Assertions.assertEquals(1, updatedHangars.size());
		selectedHangars.forEach(selectedHangar -> Assertions.assertEquals(hangar.getNumero(), selectedHangar.getNumero()));
	}
	
	/**
	 * En este test tratamos de actualizar los datos de una misma entidad concurrentemente.
	 * La entidad Persona implementa locking optimista mediante el mecanismo de versionado.
	 *
	 * De las tres modificaciones que se realizan al mismo tiempo sobre la version 0 de la entidad,
	 * solo 1 debería concluir exitosamente, quedando la entidad en la versión 1.
	 */
	@Test
	void optimisticLockTest() {
		List<Future<Persona>> futureList = Lists.newArrayList();
		Persona entity = this.createAndSavePersona();
		Long id = entity.getId();
		Long version = entity.getVersion();
		
		int writeSize = 3;
		
		for (int i = 0; i < writeSize; i++) {
			futureList.add(updateExecutorService.submit(new UpdateByIdCallable<>(personaService, id, createPersona())));
		}
		
		List<Persona> personas = resolveFutures(futureList);
		Assertions.assertEquals(1, personas.size());
		Assertions.assertEquals(version + 1, personas.get(0).getVersion());
	}
	
	/**
	 * En este test usamos el mecanismo de locking pesimista.
	 * En este tipo de mecanismos ponemos un "lock" o pausa a otras transacciones que hace que no puedan
	 * modificar ni eliminar los datos que reservamos para nuestra transacción.
	 * 
	 * El método {@link HangarService#update} tiene configurado por anotación un nivel de isolation que hace
	 * que solo 1 transacción pueda agarrar el lock exclusivo sobre los datos.
	 */
	@Test
	void pessimisticLockTest() {
		List<Future<Hangar>> futureList = Lists.newArrayList();
		Hangar hangar = createAndSaveHangar();
		Long id = hangar.getId();
		
		int writeSize = 3;
		
		for (int i = 0; i < writeSize; i++) {
			futureList.add(updateExecutorService.submit(new UpdateByIdCallable<>(hangarService, id, createHangar())));
		}
		
		List<Hangar> hangars = resolveFutures(futureList);
		Assertions.assertEquals(1, hangars.size());
	}
	
	private Persona createAndSavePersona() {
		return this.personaService.create(createPersona());
	}
	
	private Persona createPersona() {
		Persona body = new Persona();
		body.setNombre(TestUtils.getString());
		body.setApellido(TestUtils.getString());
		return body;
	}
	
	private Hangar createAndSaveHangar() {
		return this.hangarService.create(createHangar());
	}
	
	private Hangar createHangar() {
		Hangar hangar = new Hangar();
		
		hangar.setNumero(TestUtils.getLong(1, 1000));
		hangar.setCapacidad(TestUtils.getLong(1, 10));
		hangar.setLatitud(BigDecimal.valueOf(TestUtils.getDouble(-90, 90)));
		hangar.setLongitud(BigDecimal.valueOf(TestUtils.getDouble(-180, 180)));
		hangar.setAviones(Lists.newArrayList());
		
		return hangar;
	}
	
	private <E extends Entity> List<E> resolveFutures(List<Future<E>> futureList) {
		List<E> entities = Lists.newArrayList();
		futureList.forEach(future -> {
			E entity = null;
			
			try {
				entity = future.get(1, TimeUnit.SECONDS);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			if (entity != null) {
				entities.add(entity);
			}
		});
		return entities;
	}
	
}
