package com.bbdd.aeropuerto;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.jeasy.random.EasyRandom;

public class TestUtils {

	private static EasyRandom random = new EasyRandom();
	
	static Long getLong(long from, long to) {
		return random.longs(from, to).findFirst().getAsLong();
	}
	
	static String getString() {
		return random.nextObject(String.class);
	}
	
	public static LocalDateTime getLocalDateTime() {
		return random.nextObject(LocalDateTime.class);
	}
	
	public static Double getDouble(double from, double to) {
		return random.doubles(from, to).findFirst().getAsDouble();
	}
	
	public static LocalDate getLocalDate() {
		return random.nextObject(LocalDate.class);
	}
	
	public static LocalTime getLocalTime() {
		return random.nextObject(LocalTime.class);
	}
}
