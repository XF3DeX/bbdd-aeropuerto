package com.bbdd.aeropuerto;

import java.math.BigDecimal;

import javax.transaction.Transactional;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.util.Assert;
import com.bbdd.aeropuerto.model.Aeroplano;
import com.bbdd.aeropuerto.model.Avion;
import com.bbdd.aeropuerto.model.EmplMantenimiento;
import com.bbdd.aeropuerto.model.Hangar;
import com.bbdd.aeropuerto.model.Modelo;
import com.bbdd.aeropuerto.model.Persona;
import com.bbdd.aeropuerto.model.Piloto;
import com.bbdd.aeropuerto.model.Reporte;
import com.bbdd.aeropuerto.model.Tarea;
import com.bbdd.aeropuerto.model.TareaEmpleado;
import com.bbdd.aeropuerto.repository.AvionRepository;
import com.bbdd.aeropuerto.repository.EmpleadoMantenimientoRepository;
import com.bbdd.aeropuerto.repository.PersonaRepository;
import com.bbdd.aeropuerto.repository.PilotoRepository;
import com.bbdd.aeropuerto.repository.ReporteRepository;
import com.bbdd.aeropuerto.repository.TareaEmpleadoRepository;

@SpringBootTest
class AeropuertoApplicationTests {

	@Autowired
	private PersonaRepository personaRepository;
	
	@Autowired
	private EmpleadoMantenimientoRepository empleadoMantenimientoRepository;
	
	@Autowired
	private PilotoRepository pilotoRepository;
	
	@Autowired
	private AvionRepository avionRepository;
	
	@Autowired
	private ReporteRepository reporteRepository;
	
	@Autowired
	private TareaEmpleadoRepository tareaEmpleadoRepository;
	
	@Test
	void contextLoads() {
		Assert.notNull(this.personaRepository);
	}
	
	@Test
	@Rollback
	@Disabled
	void saveTest() {
		EmplMantenimiento emplMantenimiento = new EmplMantenimiento();
		emplMantenimiento.setApellido("Empleado");
		emplMantenimiento.setNombre("Federico");
		emplMantenimiento.setTareas(Lists.newArrayList());
		emplMantenimiento.setSueldoFijo(BigDecimal.TEN);
		this.empleadoMantenimientoRepository.save(emplMantenimiento);
		
		Persona persona = new Persona();
		persona.setNombre("Federico");
		persona.setApellido("Pacheco");
		this.personaRepository.save(persona);
		
		Assert.notNull(emplMantenimiento.getId());
		Assert.isTrue(!this.personaRepository.findAll().isEmpty());
	}
	
	@Test
	@Transactional
	@Rollback(false)
	void completeTest() {
		Avion avion = new Avion();
		Hangar hangar = new Hangar();
		Modelo modelo = new Modelo();
		
		modelo.setCapacidad(TestUtils.getLong(100, 500));
		modelo.setNumero(TestUtils.getLong(0, 1000));
		modelo.setPeso(BigDecimal.valueOf(TestUtils.getLong(1000000, 2000000)));
		
		avion.setNumeroRegistro(TestUtils.getLong(2000, 3000));
		avion.setNombre(TestUtils.getString());
		avion.setFechaPrimerVuelo(TestUtils.getLocalDateTime());
		avion.setHangar(hangar);
		avion.setModelo(modelo);
		
		hangar.setNumero(TestUtils.getLong(3000, 4000));
		hangar.setCapacidad(TestUtils.getLong(10, 50));
		hangar.setLatitud(BigDecimal.valueOf(TestUtils.getDouble(-90, 90)));
		hangar.setLongitud(BigDecimal.valueOf(TestUtils.getDouble(-180, 180)));
		hangar.setAviones(Lists.newArrayList(avion));
		
		this.avionRepository.save(avion);
		
		Assert.notNull(avion.getId());
		Assert.notNull(hangar.getId());
		Assert.notNull(modelo.getId());
		
		
		Persona dueño = new Persona();
		Tarea tarea1 = new Tarea();
		Tarea tarea2 = new Tarea();
		EmplMantenimiento emplMant1 = new EmplMantenimiento();
		TareaEmpleado tareaEmpleado1 = new TareaEmpleado();
		TareaEmpleado tareaEmpleado2 = new TareaEmpleado();
		Aeroplano aeroplano = new Aeroplano();
		Piloto piloto = new Piloto();
		Reporte reporte = new Reporte();
		
		dueño.setNombre(TestUtils.getString());
		dueño.setApellido(TestUtils.getString());
		
		tarea1.setNombre(TestUtils.getString());
		tarea1.setDescripcion(TestUtils.getString());
		
		tarea2.setNombre(TestUtils.getString());
		tarea2.setDescripcion(TestUtils.getString());
		
		emplMant1.setNombre(TestUtils.getString());
		emplMant1.setApellido(TestUtils.getString());
		emplMant1.setSueldoFijo(BigDecimal.valueOf(TestUtils.getDouble(100000, 200000)));
		emplMant1.setTareas(Lists.newArrayList(tareaEmpleado1));
		
		piloto.setNombre(TestUtils.getString());
		piloto.setApellido(TestUtils.getString());
		piloto.setSueldoFijo(BigDecimal.valueOf(TestUtils.getDouble(100000, 200000)));
		piloto.setAdicionales(BigDecimal.valueOf(TestUtils.getDouble(20000, 50000)));
		piloto.setAeroplanos(Lists.newArrayList(aeroplano));
		
		reporte.setAeroplano(aeroplano);
		reporte.setFecha(TestUtils.getLocalDate());
		reporte.setHorasIncurridas(TestUtils.getLong(10, 20));
		reporte.setTareas(Lists.newArrayList(tareaEmpleado1, tareaEmpleado2));
		
		tareaEmpleado1.setEmpleado(emplMant1);
		tareaEmpleado1.setFecha(TestUtils.getLocalDate());
		tareaEmpleado1.setHoraDesde(TestUtils.getLocalTime());
		tareaEmpleado1.setHoraHasta(TestUtils.getLocalTime());
		tareaEmpleado1.setTarea(tarea1);
		tareaEmpleado1.setReporte(reporte);
		
		tareaEmpleado2.setEmpleado(emplMant1);
		tareaEmpleado2.setFecha(TestUtils.getLocalDate());
		tareaEmpleado2.setHoraDesde(TestUtils.getLocalTime());
		tareaEmpleado2.setHoraHasta(TestUtils.getLocalTime());
		tareaEmpleado2.setTarea(tarea2);
		tareaEmpleado2.setReporte(reporte);
		
		aeroplano.setNumeroRegistro(TestUtils.getLong(2000, 3000));
		aeroplano.setNombre(TestUtils.getString());
		aeroplano.setFechaPrimerVuelo(TestUtils.getLocalDateTime());
		aeroplano.setHangar(hangar);
		aeroplano.setModelo(modelo);
		aeroplano.setDueño(dueño);
		aeroplano.setEmpleados(Lists.newArrayList(emplMant1));
		aeroplano.setFechaCompra(TestUtils.getLocalDateTime());
		aeroplano.setReportes(Lists.newArrayList(reporte));
		
		this.avionRepository.save(aeroplano);
		this.pilotoRepository.save(piloto);
		
		Assert.notNull(dueño.getId());
		Assert.notNull(emplMant1.getId());
		Assert.notNull(tareaEmpleado1.getId());
		Assert.notNull(tareaEmpleado2.getId());
		Assert.notNull(tarea1.getId());
		Assert.notNull(tarea2.getId());
		Assert.notNull(aeroplano.getId());
		Assert.notNull(piloto.getId());
		Assert.notNull(reporte.getId());
	}

}
