package com.bbdd.aeropuerto.concurrency;

import com.bbdd.aeropuerto.model.Entity;
import com.bbdd.aeropuerto.service.EntityService;

public class UpdateByIdCallable<E extends Entity> extends EntityCallable<E> {
	
	private Long id;
	private E body;
	
	public UpdateByIdCallable(EntityService<E> service, Long id, E body) {
		super(service);
		this.id = id;
		this.body = body;
	}
	
	@Override
	public E call() throws Exception {
		return this.service.update(this.id, this.body);
	}
}
