package com.bbdd.aeropuerto.concurrency;

import com.bbdd.aeropuerto.model.Entity;
import com.bbdd.aeropuerto.service.EntityService;

public class FindByIdCallable<E extends Entity> extends EntityCallable<E> {
	
	private Long id;
	
	public FindByIdCallable(EntityService<E> service, Long id) {
		super(service);
		this.id = id;
	}
	
	@Override
	public E call() throws Exception {
		return this.service.findById(id);
	}
	
}
