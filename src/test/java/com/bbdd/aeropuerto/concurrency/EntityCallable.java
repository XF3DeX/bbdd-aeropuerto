package com.bbdd.aeropuerto.concurrency;

import java.util.concurrent.Callable;

import com.bbdd.aeropuerto.model.Entity;
import com.bbdd.aeropuerto.service.EntityService;

public abstract class EntityCallable<E extends Entity> implements Callable<E> {
	
	EntityService<E> service;
	
	public EntityCallable(EntityService<E> service) {
		this.service = service;
	}
	
}
