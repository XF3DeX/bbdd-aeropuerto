package com.bbdd.aeropuerto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.bbdd.aeropuerto.model.Hangar;
import com.bbdd.aeropuerto.model.Persona;
import com.bbdd.aeropuerto.service.HangarService;

/**
 * <p>Esta aplicación fue hecha por Federico Pacheco para la Cátedra de Bases de Datos 2 de la Facultad de Informática de la UNLP.</p>
 *
 * <p>La idea es presentar una aplicación que se conecte a una base de datos y resuelva el problema de la concurrencia en una aplicación moderna, no armar
 * una aplicación web completa.</p>
 * <p>En el alcance de esta aplicación se incluye:</p>
 * <ul>
 *     <li>Base de datos para mysql (se provee un script para recrear la db)</li>
 *     <li>Algunos scripts para probar requests con curl desde bash</li>
 *     <li>Collection de algunas requests listas para probar casos de entidades Persona y Hangar</li>
 *     <li>Aplicación hecha con spring-boot, hibernate, jackson, jersey</li>
 *     <li>Modelo y mapeo para toda la base de datos usando hibernate</li>
 *     <li>Servicios y controladores para todas las entidades del modelo</li>
 *     <li>Tests en JUnit probando persistencia de cada uno de los modelos</li>
 *     <li>Tests en JUnit probando lectura y modificación concurrente de algunos modelos</li>
 * </ul>
 * <p>Para resolver el problema de la concurrencia se muestran dos estrategias:</p>
 * <ul>
 *     <li>
 *         <p>Locking Optimista</p>
 *         <p>Para el caso de personas (jerarquía {@link Persona}) se utiliza el mecanismo de versionado de entidades.</p>
 *         <p>En este proyecto solo mostramos el uso de versionado ya que nos lo provee la api de JPA,
 *         pero también podríamos haber realizado esto manualmente usando la sentencia <pre>UPDATE tabla set a=? WHERE id=? and version=?</pre></p>
 *     </li>
 *     <li>
 *         <p>Locking Pesimista</p>
 *         <p>Para el caso de los hangares ({@link Hangar}) usamos locking pesimista cambiando el nivel de isolation la transacción creada por
 *         Hibernate para el método {@link HangarService#update}</p>
 *         <p>Otra forma de locking pesimista se ve en {@link HangarService#findById} donde podemos obtener un lock compartido sobre los datos,
 *         perimitiendo leer pero no escribir mientras dura la transacción de lectura de la entidad.</p>
 *     </li>
 * </ul>
 *
 */
@SpringBootApplication
public class AeropuertoApplication {

	public static void main(String[] args) {
		SpringApplication.run(AeropuertoApplication.class, args);
	}

}
