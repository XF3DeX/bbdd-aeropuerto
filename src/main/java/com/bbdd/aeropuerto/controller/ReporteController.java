package com.bbdd.aeropuerto.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.bbdd.aeropuerto.model.Reporte;
import com.bbdd.aeropuerto.service.ReporteService;

@RestController
@RequestMapping("/reporte")
public class ReporteController extends EntityController<Reporte> {
	@Autowired
	ReporteController(ReporteService service) {
		super(service);
	}
}
