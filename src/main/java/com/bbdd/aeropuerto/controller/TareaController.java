package com.bbdd.aeropuerto.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.bbdd.aeropuerto.model.Tarea;
import com.bbdd.aeropuerto.service.TareaService;

@RestController
@RequestMapping("/tarea")
public class TareaController extends EntityController<Tarea> {
	@Autowired
	TareaController(TareaService service) {
		super(service);
	}
}
