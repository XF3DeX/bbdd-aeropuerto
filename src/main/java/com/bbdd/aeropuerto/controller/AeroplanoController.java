package com.bbdd.aeropuerto.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.bbdd.aeropuerto.model.Aeroplano;
import com.bbdd.aeropuerto.service.AeroplanoService;

@RestController
@RequestMapping("/aeroplano")
public class AeroplanoController extends EntityController<Aeroplano> {
	
	@Autowired
	AeroplanoController(AeroplanoService service) {
		super(service);
	}
	
}
