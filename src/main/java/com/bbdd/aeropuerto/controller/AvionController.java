package com.bbdd.aeropuerto.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.bbdd.aeropuerto.model.Avion;
import com.bbdd.aeropuerto.service.AvionService;

@RestController
@RequestMapping("/avion")
public class AvionController extends EntityController<Avion> {
	@Autowired
	AvionController(AvionService service) {
		super(service);
	}
}
