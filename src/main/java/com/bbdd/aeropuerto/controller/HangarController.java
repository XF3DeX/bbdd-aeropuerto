package com.bbdd.aeropuerto.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.bbdd.aeropuerto.model.Hangar;
import com.bbdd.aeropuerto.service.HangarService;

@RestController
@RequestMapping("/hangar")
public class HangarController extends EntityController<Hangar> {
	
	@Autowired
	HangarController(HangarService service) {
		super(service);
	}
	
}
