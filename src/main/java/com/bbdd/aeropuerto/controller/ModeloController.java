package com.bbdd.aeropuerto.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.bbdd.aeropuerto.model.Modelo;
import com.bbdd.aeropuerto.service.ModeloService;

@RestController
@RequestMapping("/modelo")
public class ModeloController extends EntityController<Modelo> {
	@Autowired
	ModeloController(ModeloService service) {
		super(service);
	}
}
