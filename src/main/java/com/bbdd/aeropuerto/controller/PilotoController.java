package com.bbdd.aeropuerto.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.bbdd.aeropuerto.model.Piloto;
import com.bbdd.aeropuerto.service.EntityService;

@RestController
@RequestMapping("/piloto")
public class PilotoController extends EntityController<Piloto> {
	@Autowired
	PilotoController(EntityService<Piloto> service) {
		super(service);
	}
}
