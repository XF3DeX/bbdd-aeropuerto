package com.bbdd.aeropuerto.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.bbdd.aeropuerto.model.EmplMantenimiento;
import com.bbdd.aeropuerto.service.EmplMantenimientoServicio;

@RestController
@RequestMapping("/empl-mantenimiento")
public class EmplMantenimientoController extends EntityController<EmplMantenimiento> {
	@Autowired
	EmplMantenimientoController(EmplMantenimientoServicio service) {
		super(service);
	}
}
