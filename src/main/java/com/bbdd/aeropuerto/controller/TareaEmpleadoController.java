package com.bbdd.aeropuerto.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.bbdd.aeropuerto.model.TareaEmpleado;
import com.bbdd.aeropuerto.service.TareaEmpleadoService;

@RestController
@RequestMapping("/tarea-empleado")
public class TareaEmpleadoController extends EntityController<TareaEmpleado> {
	@Autowired
	TareaEmpleadoController(TareaEmpleadoService service) {
		super(service);
	}
}
