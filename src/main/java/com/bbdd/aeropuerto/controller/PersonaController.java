package com.bbdd.aeropuerto.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.bbdd.aeropuerto.model.Persona;
import com.bbdd.aeropuerto.service.PersonaService;

@RestController
@RequestMapping("/persona")
public class PersonaController extends EntityController<Persona> {
	
	@Autowired
	public PersonaController(PersonaService service) {
		super(service);
	}
}
