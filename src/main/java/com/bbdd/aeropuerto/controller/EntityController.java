package com.bbdd.aeropuerto.controller;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.bbdd.aeropuerto.model.Entity;
import com.bbdd.aeropuerto.service.AeropuertoException;
import com.bbdd.aeropuerto.service.EntityService;

@RestController
public abstract class EntityController<E extends Entity> {
	
	private EntityService<E> service;
	
	EntityController(EntityService<E> service) {
		this.service = service;
	}
	
	@GetMapping
	public List<E> findAll() {
		return this.service.findAll();
	}
	
	@GetMapping("/{id}")
	public E findById(@PathVariable("id") Long id) throws AeropuertoException {
		return this.service.findById(id);
	}
	
	@PostMapping
	public E create(@RequestBody E body) {
		return this.service.create(body);
	}
	
	@PutMapping("/{id}")
	public E update(@PathVariable("id") Long id, @RequestBody E body) throws AeropuertoException {
		return this.service.update(id, body);
	}
	
	@DeleteMapping("/{id}")
	public E delete(@PathVariable("id") Long id) throws AeropuertoException {
		return this.service.delete(id);
	}
	
}
