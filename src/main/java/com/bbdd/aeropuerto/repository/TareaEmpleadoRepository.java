package com.bbdd.aeropuerto.repository;

import com.bbdd.aeropuerto.model.TareaEmpleado;

public interface TareaEmpleadoRepository extends EntityRepository<TareaEmpleado> {

}
