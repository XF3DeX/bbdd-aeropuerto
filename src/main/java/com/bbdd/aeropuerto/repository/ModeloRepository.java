package com.bbdd.aeropuerto.repository;

import com.bbdd.aeropuerto.model.Modelo;

public interface ModeloRepository extends EntityRepository<Modelo> {
}
