package com.bbdd.aeropuerto.repository;

import com.bbdd.aeropuerto.model.EmplMantenimiento;

public interface EmpleadoMantenimientoRepository extends EntityRepository<EmplMantenimiento> {

}
