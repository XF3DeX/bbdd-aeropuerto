package com.bbdd.aeropuerto.repository;

import com.bbdd.aeropuerto.model.Reporte;

public interface ReporteRepository extends EntityRepository<Reporte> {
}
