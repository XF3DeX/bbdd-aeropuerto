package com.bbdd.aeropuerto.repository;

import com.bbdd.aeropuerto.model.Aeroplano;

public interface AeroplanoRepository extends EntityRepository<Aeroplano> {
}
