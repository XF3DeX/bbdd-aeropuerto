package com.bbdd.aeropuerto.repository;

import com.bbdd.aeropuerto.model.Tarea;

public interface TareaRepository extends EntityRepository<Tarea> {
}
