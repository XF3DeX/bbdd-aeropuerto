package com.bbdd.aeropuerto.repository;

import com.bbdd.aeropuerto.model.Avion;

public interface AvionRepository extends EntityRepository<Avion> {

}
