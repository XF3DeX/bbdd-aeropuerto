package com.bbdd.aeropuerto.repository;

import com.bbdd.aeropuerto.model.Hangar;

public interface HangarRepository extends EntityRepository<Hangar> {
}
