package com.bbdd.aeropuerto.repository;

import com.bbdd.aeropuerto.model.Piloto;

public interface PilotoRepository extends EntityRepository<Piloto> {

}
