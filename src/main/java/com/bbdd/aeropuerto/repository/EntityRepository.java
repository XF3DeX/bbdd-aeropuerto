package com.bbdd.aeropuerto.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.bbdd.aeropuerto.model.Entity;

public interface EntityRepository<E extends Entity> extends JpaRepository<E, Long> {
}
