package com.bbdd.aeropuerto.repository;

import org.springframework.stereotype.Repository;
import com.bbdd.aeropuerto.model.Persona;

@Repository
public interface PersonaRepository extends EntityRepository<Persona> {

}
