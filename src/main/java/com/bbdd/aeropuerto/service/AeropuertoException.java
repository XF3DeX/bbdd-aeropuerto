package com.bbdd.aeropuerto.service;

public class AeropuertoException extends Exception {
	
	public AeropuertoException(String message) {
		super(message);
	}
	
}
