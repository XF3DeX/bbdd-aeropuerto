package com.bbdd.aeropuerto.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bbdd.aeropuerto.model.EmplMantenimiento;
import com.bbdd.aeropuerto.repository.EmpleadoMantenimientoRepository;

@Service
public class EmplMantenimientoServicio extends EntityService<EmplMantenimiento> {
	
	@Autowired
	public EmplMantenimientoServicio(EmpleadoMantenimientoRepository repository) {
		super(repository);
	}
	
	@Override
	EmplMantenimiento createFromBody(EmplMantenimiento body) {
		return body;
	}
	
	@Override
	EmplMantenimiento updateFromBody(EmplMantenimiento entity, EmplMantenimiento body) {
		if (body.getNombre() != null) {
			entity.setNombre(body.getNombre());
		}
		if (body.getApellido() != null) {
			entity.setApellido(body.getApellido());
		}
		if (body.getSueldoFijo() != null) {
			entity.setSueldoFijo(body.getSueldoFijo());
		}

		return entity;
	}
	
}
