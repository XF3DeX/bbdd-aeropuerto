package com.bbdd.aeropuerto.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bbdd.aeropuerto.model.Tarea;
import com.bbdd.aeropuerto.repository.TareaRepository;

@Service
public class TareaService extends EntityService<Tarea> {
	
	@Autowired
	public TareaService(TareaRepository repository) {
		super(repository);
	}
	
	@Override
	Tarea createFromBody(Tarea body) {
		return body;
	}
	
	@Override
	Tarea updateFromBody(Tarea entity, Tarea body) {
		if (body.getNombre() != null) {
			entity.setNombre(body.getNombre());
		}
		if (body.getDescripcion() != null) {
			entity.setDescripcion(body.getDescripcion());
		}
		return entity;
	}
}
