package com.bbdd.aeropuerto.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bbdd.aeropuerto.model.Persona;
import com.bbdd.aeropuerto.repository.PersonaRepository;

@Service
public class PersonaService extends EntityService<Persona> {
	
	@Autowired
	public PersonaService(PersonaRepository repository) {
		super(repository);
	}
	
	@Override
	Persona createFromBody(Persona body) {
		return body;
	}
	
	@Override
	Persona updateFromBody(Persona entity, Persona body) {
		if (body.getNombre() != null) {
			entity.setNombre(body.getNombre());
		}
		if (body.getApellido() != null) {
			entity.setApellido(body.getApellido());
		}
		return entity;
	}
	
}
