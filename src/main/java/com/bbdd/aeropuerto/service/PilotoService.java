package com.bbdd.aeropuerto.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bbdd.aeropuerto.model.Piloto;
import com.bbdd.aeropuerto.repository.PilotoRepository;

@Service
public class PilotoService extends EntityService<Piloto> {
	
	@Autowired
	public PilotoService(PilotoRepository repository) {
		super(repository);
	}
	
	@Override
	Piloto createFromBody(Piloto body) {
		return body;
	}
	
	@Override
	Piloto updateFromBody(Piloto entity, Piloto body) {
		if (body.getNombre() != null) {
			entity.setNombre(body.getNombre());
		}
		if (body.getApellido() != null) {
			entity.setApellido(body.getApellido());
		}
		if (body.getSueldoFijo() != null) {
			entity.setSueldoFijo(body.getSueldoFijo());
		}
		if (body.getAdicionales() != null) {
			entity.setAdicionales(body.getAdicionales());
		}
		return entity;
	}
}
