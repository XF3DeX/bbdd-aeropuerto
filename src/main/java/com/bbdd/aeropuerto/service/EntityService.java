package com.bbdd.aeropuerto.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import com.bbdd.aeropuerto.model.Entity;
import com.bbdd.aeropuerto.repository.EntityRepository;

public abstract class EntityService<E extends Entity> {
	
	protected EntityRepository<E> repository;
	
	public EntityService(EntityRepository<E> repository) {
		this.repository = repository;
	}
	
	@Transactional
	public List<E> findAll() {
		List<E> all = this.repository.findAll();
		this.initializeAll(all);
		return all;
	}
	
	@Transactional
	public E findById(Long id) throws AeropuertoException {
		E entity = this.findOrFail(id);
		this.initialize(entity);
		return entity;
	}
	
	@Transactional
	public E create(E body) {
		E entity = this.createFromBody(body);
		return this.repository.save(entity);
	}
	
	@Transactional
	public E update(Long id, E body) throws AeropuertoException {
		E entity = this.findOrFail(id);
		this.initialize(entity);
		entity = updateFromBody(entity, body);
		return this.repository.save(entity);
	}
	
	@Transactional
	public E delete(Long id) throws AeropuertoException {
		E entity = this.findOrFail(id);
		this.initialize(entity);
		this.repository.delete(entity);
		return entity;
	}
	
	abstract E createFromBody(E body);
	
	abstract E updateFromBody(E entity, E body);
	
	/**
	 * Initializes lazy collections when fetching all entities
	 * @param all
	 */
	void initializeAll(List<E> all) {
		for (E entity : all) {
			this.initialize(entity);
		}
	}
	
	/**
	 * Initializes lazy collections when fetching a particular entity
	 * @param entity
	 */
	void initialize(E entity) {
	
	}
	
	private E findOrFail(Long id) throws AeropuertoException {
		return this.repository.findById(id).orElseThrow(() -> new AeropuertoException("Entity not found"));
	}
	
}
