package com.bbdd.aeropuerto.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bbdd.aeropuerto.model.Reporte;
import com.bbdd.aeropuerto.repository.ReporteRepository;

@Service
public class ReporteService extends EntityService<Reporte> {
	
	@Autowired
	public ReporteService(ReporteRepository repository) {
		super(repository);
	}
	
	@Override
	Reporte createFromBody(Reporte body) {
		return body;
	}
	
	@Override
	Reporte updateFromBody(Reporte entity, Reporte body) {
		if (body.getFecha() != null) {
			entity.setFecha(body.getFecha());
		}
		if (body.getHorasIncurridas() != null) {
			entity.setHorasIncurridas(body.getHorasIncurridas());
		}
		
		return entity;
	}
}
