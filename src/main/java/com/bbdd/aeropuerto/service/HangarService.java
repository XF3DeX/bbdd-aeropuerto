package com.bbdd.aeropuerto.service;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import com.bbdd.aeropuerto.model.Hangar;
import com.bbdd.aeropuerto.repository.HangarRepository;

@Service
public class HangarService extends EntityService<Hangar> {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	public HangarService(HangarRepository repository) {
		super(repository);
	}
	
	@Override
	Hangar createFromBody(Hangar body) {
		return body;
	}
	
	@Override
	Hangar updateFromBody(Hangar entity, Hangar body) {
		if (body.getNumero() != null) {
			entity.setNumero(body.getNumero());
		}
		if (body.getCapacidad() != null) {
			entity.setCapacidad(body.getCapacidad());
		}
		if (body.getLatitud() != null) {
			entity.setLatitud(body.getLatitud());
		}
		if (body.getLongitud() != null) {
			entity.setLongitud(body.getLongitud());
		}
		return entity;
	}
	
	@Transactional
	@Override
	public Hangar findById(Long id) throws AeropuertoException {
		return this.entityManager.find(Hangar.class, id, LockModeType.PESSIMISTIC_READ);
	}
	
	/**
	 * Con este nivel de isolation vamos a conseguir que se lockee tanto lecturas como escrituras sobre los datos afectados.
	 * Esto quiere decir que cada transacción se va a ejecutar secuencialmente, afectando la performance.
	 *
	 * Cuando dos transacciones quieren agarrar el lock sobre los datos, solo 1 podrá hacerlo. La transacción perdedora
	 * tendrá que hacer rollback para reintentarla en el futuro.
	 */
	@Transactional(isolation = Isolation.SERIALIZABLE)
	@Override
	public Hangar update(Long id, Hangar body) throws AeropuertoException {
		return super.update(id, body);
	}
}
