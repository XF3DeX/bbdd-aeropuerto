package com.bbdd.aeropuerto.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bbdd.aeropuerto.model.Avion;
import com.bbdd.aeropuerto.repository.AvionRepository;

@Service
public class AvionService extends EntityService<Avion> {
	
	@Autowired
	public AvionService(AvionRepository repository) {
		super(repository);
	}
	
	@Override
	Avion createFromBody(Avion body) {
		return body;
	}
	
	@Override
	Avion updateFromBody(Avion entity, Avion body) {
		if (body.getNumeroRegistro() != null) {
			entity.setNumeroRegistro(body.getNumeroRegistro());
		}
		if (body.getNombre() != null) {
			entity.setNombre(body.getNombre());
		}
		if (entity.getFechaPrimerVuelo() != null) {
			entity.setFechaPrimerVuelo(body.getFechaPrimerVuelo());
		}
		
		return entity;
	}
	
}
