package com.bbdd.aeropuerto.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bbdd.aeropuerto.model.Aeroplano;
import com.bbdd.aeropuerto.repository.AeroplanoRepository;

@Service
public class AeroplanoService extends EntityService<Aeroplano> {
	
	@Autowired
	public AeroplanoService(AeroplanoRepository repository) {
		super(repository);
	}
	
	@Override
	Aeroplano createFromBody(Aeroplano body) {
		return body;
	}
	
	@Override
	Aeroplano updateFromBody(Aeroplano entity, Aeroplano body) {
		if (body.getNumeroRegistro() != null) {
			entity.setNumeroRegistro(body.getNumeroRegistro());
		}
		if (body.getNombre() != null) {
			entity.setNombre(body.getNombre());
		}
		if (entity.getFechaPrimerVuelo() != null) {
			entity.setFechaPrimerVuelo(body.getFechaPrimerVuelo());
		}
		return entity;
	}
	
}
