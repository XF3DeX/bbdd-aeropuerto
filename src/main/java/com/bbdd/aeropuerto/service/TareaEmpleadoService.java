package com.bbdd.aeropuerto.service;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bbdd.aeropuerto.model.TareaEmpleado;
import com.bbdd.aeropuerto.repository.TareaEmpleadoRepository;

@Service
public class TareaEmpleadoService extends EntityService<TareaEmpleado> {
	
	@Autowired
	public TareaEmpleadoService(TareaEmpleadoRepository repository) {
		super(repository);
	}
	
	@Override
	TareaEmpleado createFromBody(TareaEmpleado body) {
		return body;
	}
	
	@Override
	TareaEmpleado updateFromBody(TareaEmpleado entity, TareaEmpleado body) {
		if (body.getFecha() != null) {
			entity.setFecha(body.getFecha());
		}
		if (body.getHoraDesde() != null) {
			entity.setHoraHasta(body.getHoraHasta());
		}
		if (body.getHoraHasta() != null) {
			entity.setHoraHasta(body.getHoraHasta());
		}
		return entity;
	}
	
	@Override
	void initialize(TareaEmpleado entity) {
		Hibernate.initialize(entity.getEmpleado());
		Hibernate.initialize(entity.getReporte());
		Hibernate.initialize(entity.getTarea());
	}
}
