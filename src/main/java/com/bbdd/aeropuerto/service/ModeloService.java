package com.bbdd.aeropuerto.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bbdd.aeropuerto.model.Modelo;
import com.bbdd.aeropuerto.repository.ModeloRepository;

@Service
public class ModeloService extends EntityService<Modelo> {
	
	@Autowired
	public ModeloService(ModeloRepository repository) {
		super(repository);
	}
	
	@Override
	Modelo createFromBody(Modelo body) {
		return body;
	}
	
	@Override
	Modelo updateFromBody(Modelo entity, Modelo body) {
		if (body.getCapacidad() != null) {
			entity.setCapacidad(body.getCapacidad());
		}
		if (body.getNumero() != null) {
			entity.setNumero(body.getNumero());
		}
		if (body.getPeso() != null) {
			entity.setPeso(body.getPeso());
		}
		return entity;
	}
}
