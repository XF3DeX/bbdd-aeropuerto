package com.bbdd.aeropuerto.model;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * <p>La entidad Persona se utiliza para referirse a personas (nombre, apellido)
 * Además es posible que las personas sean empleados, en cuyo caso hay dos tipos: piloto y de mantenimiento.</p>
 *
 * <p>En el caso de la entidad Persona, para evitar modificaciones concurrentes se utiliza
 * el mecanismo de locking optimista. Esto consiste en guardar como parte de la entidad un número de versión.
 * Con cada guardado se genera una query de la siguiente forma:</p>
 *
 * <pre>update persona set apellido=?, nombre=?, version=n+1, sueldo_fijo=? where id=? and version=n</pre>
 *
 * <p>Para poder escribir los datos en la db estamos checkeando que la versión sea la correcta. Si otra
 * transacción anterior modificó la versión de los datos en la DB, la nueva instrucción update falla y por lo tanto la transacción no se realiza.</p>
 *
 * <p>Este tipo de locking se realiza por lo general con datos/servicios que se modifican datos constantemente,
 * con mayor probabilidad de que haya colisiones. Además estas transacciones son por lo general de baja prioridad,
 * con lo cual pueden reintentarse nuevamente si es que fallan.</p>
 */
@javax.persistence.Entity
@Table(name = "persona")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type")
public class Persona extends Entity {
	
	@NotNull
	private String nombre;
	
	@NotNull
	private String apellido;
	
	@Version
	@JsonIgnore
	private Long version;
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getApellido() {
		return apellido;
	}
	
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	public Long getVersion() {
		return version;
	}
	
	public void setVersion(Long version) {
		this.version = version;
	}
}
