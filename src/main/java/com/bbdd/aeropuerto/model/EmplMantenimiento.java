package com.bbdd.aeropuerto.model;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

@Entity
public class EmplMantenimiento extends Empleado {
	
	@OneToMany(mappedBy = "empleado", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<TareaEmpleado> tareas;
	
	public List<TareaEmpleado> getTareas() {
		return tareas;
	}
	
	public void setTareas(List<TareaEmpleado> tareas) {
		this.tareas = tareas;
	}
}
