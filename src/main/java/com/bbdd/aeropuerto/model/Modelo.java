package com.bbdd.aeropuerto.model;

import java.math.BigDecimal;

@javax.persistence.Entity
public class Modelo extends Entity {
	
	private Long numero;
	private Long capacidad;
	private BigDecimal peso;
	
	public Long getNumero() {
		return numero;
	}
	
	public void setNumero(Long numero) {
		this.numero = numero;
	}
	
	public Long getCapacidad() {
		return capacidad;
	}
	
	public void setCapacidad(Long capacidad) {
		this.capacidad = capacidad;
	}
	
	public BigDecimal getPeso() {
		return peso;
	}
	
	public void setPeso(BigDecimal peso) {
		this.peso = peso;
	}
}
