package com.bbdd.aeropuerto.model;

import java.math.BigDecimal;
import javax.persistence.Entity;

@Entity
public abstract class Empleado extends Persona {
	
	private BigDecimal sueldoFijo;
	
	public BigDecimal getSueldoFijo() {
		return sueldoFijo;
	}
	
	public void setSueldoFijo(BigDecimal sueldoFijo) {
		this.sueldoFijo = sueldoFijo;
	}
}
