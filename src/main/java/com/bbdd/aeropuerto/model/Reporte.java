package com.bbdd.aeropuerto.model;

import java.time.LocalDate;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@javax.persistence.Entity
public class Reporte extends Entity {
	
	@ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "aeroplano_id")
	private Aeroplano aeroplano;
	private LocalDate fecha;
	private Long horasIncurridas;
	
	@OneToMany(mappedBy = "reporte", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<TareaEmpleado> tareas;
	
	public Aeroplano getAeroplano() {
		return aeroplano;
	}
	
	public void setAeroplano(Aeroplano aeroplano) {
		this.aeroplano = aeroplano;
	}
	
	public LocalDate getFecha() {
		return fecha;
	}
	
	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}
	
	public Long getHorasIncurridas() {
		return horasIncurridas;
	}
	
	public void setHorasIncurridas(Long horasIncurridas) {
		this.horasIncurridas = horasIncurridas;
	}
	
	public List<TareaEmpleado> getTareas() {
		return tareas;
	}
	
	public void setTareas(List<TareaEmpleado> tareas) {
		this.tareas = tareas;
	}
}
