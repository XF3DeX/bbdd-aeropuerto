package com.bbdd.aeropuerto.model;

import java.time.LocalDateTime;
import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@javax.persistence.Entity
@Table(name = "avion")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type")
public class Avion extends Entity {
	
	private String nombre;
	private LocalDateTime fechaPrimerVuelo;
	private Long numeroRegistro;
	
	@ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "modelo_id")
	private Modelo modelo;
	
	@ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "hangar_id")
	private Hangar hangar;
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public LocalDateTime getFechaPrimerVuelo() {
		return fechaPrimerVuelo;
	}
	
	public void setFechaPrimerVuelo(LocalDateTime fechaPrimerVuelo) {
		this.fechaPrimerVuelo = fechaPrimerVuelo;
	}
	
	public Long getNumeroRegistro() {
		return numeroRegistro;
	}
	
	public void setNumeroRegistro(Long numeroRegistro) {
		this.numeroRegistro = numeroRegistro;
	}
	
	public Modelo getModelo() {
		return modelo;
	}
	
	public void setModelo(Modelo modelo) {
		this.modelo = modelo;
	}
	
	public Hangar getHangar() {
		return hangar;
	}
	
	public void setHangar(Hangar hangar) {
		this.hangar = hangar;
	}
}
