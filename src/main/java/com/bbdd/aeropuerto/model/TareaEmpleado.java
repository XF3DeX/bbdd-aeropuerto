package com.bbdd.aeropuerto.model;

import java.time.LocalDate;
import java.time.LocalTime;
import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@javax.persistence.Entity
public class TareaEmpleado extends Entity {
	
	@ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "empleado_mantenimiento_id")
	private EmplMantenimiento empleado;
	
	@ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "tarea_id")
	private Tarea tarea;
	
	@ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "reporte_id")
	private Reporte reporte;
	
	private LocalDate fecha;
	private LocalTime horaDesde;
	private LocalTime horaHasta;
	
	public EmplMantenimiento getEmpleado() {
		return empleado;
	}
	
	public void setEmpleado(EmplMantenimiento empleado) {
		this.empleado = empleado;
	}
	
	public Tarea getTarea() {
		return tarea;
	}
	
	public void setTarea(Tarea tarea) {
		this.tarea = tarea;
	}
	
	public LocalDate getFecha() {
		return fecha;
	}
	
	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}
	
	public LocalTime getHoraDesde() {
		return horaDesde;
	}
	
	public void setHoraDesde(LocalTime horaDesde) {
		this.horaDesde = horaDesde;
	}
	
	public LocalTime getHoraHasta() {
		return horaHasta;
	}
	
	public void setHoraHasta(LocalTime horaHasta) {
		this.horaHasta = horaHasta;
	}
	
	public Reporte getReporte() {
		return reporte;
	}
	
	public void setReporte(Reporte reporte) {
		this.reporte = reporte;
	}
}
