package com.bbdd.aeropuerto.model;

import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Aeroplano extends Avion {

	@ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "dueño_id")
	private Persona dueño;
	private LocalDateTime fechaCompra;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
			name = "aeroplano_empleado",
			joinColumns = @JoinColumn(name = "aeroplano_id", nullable = false),
			inverseJoinColumns = @JoinColumn(name = "empleado_mantenimiento_id", nullable = false)
	)
	private List<EmplMantenimiento> empleados;
	
	@OneToMany(mappedBy = "aeroplano", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Reporte> reportes;
	
	public Persona getDueño() {
		return dueño;
	}
	
	public void setDueño(Persona dueño) {
		this.dueño = dueño;
	}
	
	public LocalDateTime getFechaCompra() {
		return fechaCompra;
	}
	
	public void setFechaCompra(LocalDateTime fechaCompra) {
		this.fechaCompra = fechaCompra;
	}
	
	public List<EmplMantenimiento> getEmpleados() {
		return empleados;
	}
	
	public void setEmpleados(List<EmplMantenimiento> empleados) {
		this.empleados = empleados;
	}
	
	public List<Reporte> getReportes() {
		return reportes;
	}
	
	public void setReportes(List<Reporte> reportes) {
		this.reportes = reportes;
	}
}
