package com.bbdd.aeropuerto.model;

import java.math.BigDecimal;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@javax.persistence.Entity
public class Hangar extends Entity {
	
	private Long numero;
	private Long capacidad;
	private BigDecimal latitud;
	private BigDecimal longitud;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
			name = "hangar_avion",
			joinColumns = @JoinColumn(name = "hangar_id", nullable = false),
			inverseJoinColumns = @JoinColumn(name = "avion_id", nullable = false)
	)
	private List<Avion> aviones;
	
	public Long getNumero() {
		return numero;
	}
	
	public void setNumero(Long numero) {
		this.numero = numero;
	}
	
	public Long getCapacidad() {
		return capacidad;
	}
	
	public void setCapacidad(Long capacidad) {
		this.capacidad = capacidad;
	}
	
	public BigDecimal getLatitud() {
		return latitud;
	}
	
	public void setLatitud(BigDecimal latitud) {
		this.latitud = latitud;
	}
	
	public BigDecimal getLongitud() {
		return longitud;
	}
	
	public void setLongitud(BigDecimal longitud) {
		this.longitud = longitud;
	}
	
	public List<Avion> getAviones() {
		return aviones;
	}
	
	public void setAviones(List<Avion> aviones) {
		this.aviones = aviones;
	}
}
