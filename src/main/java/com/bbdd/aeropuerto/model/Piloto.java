package com.bbdd.aeropuerto.model;

import java.math.BigDecimal;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class Piloto extends Empleado {
	
	private BigDecimal adicionales;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
			name = "piloto_aeroplano",
			joinColumns = @JoinColumn(name = "piloto_id", nullable = false),
			inverseJoinColumns = @JoinColumn(name = "aeroplano_id", nullable = false)
	)
	private List<Aeroplano> aeroplanos;
	
	public BigDecimal getAdicionales() {
		return adicionales;
	}
	
	public void setAdicionales(BigDecimal adicionales) {
		this.adicionales = adicionales;
	}
	
	public List<Aeroplano> getAeroplanos() {
		return aeroplanos;
	}
	
	public void setAeroplanos(List<Aeroplano> aeroplanos) {
		this.aeroplanos = aeroplanos;
	}
}
